import os
import sys
import django
from os.path import dirname, abspath
from colorama import init, Fore


init()
path = dirname(dirname(abspath(__file__)))
sys.path.append(path)
from talismanwiki import TalismanWiki

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "talisman_cards.settings")
django.setup()
from cards.models import *

def check_sets(db_card, wiki_card, cardtype):
    db_sets = Set.objects.all()
    for wiki_set in wiki_card.copies:
        db_set = db_sets.filter(name=wiki_set['name'])
        if not db_set:
            db_set = Set(name=wiki_set['name'])
            db_set.save()
            print 'Dodano set:', wiki_set['name']
        else:
            db_set = db_set[0]
        if not db_set in db_card.sets.all():
            #print 'Przypisuje {0} do {1}'.format(db_card, db_set)
            if cardtype == Enemy:
                db_card.setenemy_set.create(set=db_set, copies=wiki_set['count'])
            elif cardtype == Stranger:
                db_card.setstranger_set.create(set=db_set, copies=wiki_set['count'])
            elif cardtype == Event:
                db_card.setevent_set.create(set=db_set, copies=wiki_set['count'])
            elif cardtype == Place:
                db_card.setplace_set.create(set=db_set, copies=wiki_set['count'])
            elif cardtype == Follower:
                db_card.setfollower_set.create(set=db_set, copies=wiki_set['count'])
            elif cardtype == Object:
                db_card.setobject_set.create(set=db_set, copies=wiki_set['count'])
            elif cardtype == MagicObject:
                db_card.setmagicobject_set.create(set=db_set, copies=wiki_set['count'])

def get_or_create_enemy(wiki_card, db_subtype):
    db_card = Enemy.objects.filter(sub_type=db_subtype,
                                    title=wiki_card.title,
                                    encounter_nr=wiki_card.encounter_nr,
                                    strength=wiki_card.strength,
                                    craft=wiki_card.craft,
                                    description=wiki_card.description)
    if len(db_card) > 1:
        txt = 'Karta "{0}" wystepuje {1} razy!'.format(wiki_card.title, len(db_card))
        raise Exception(txt)
    elif len(db_card) == 1:
        db_card = db_card[0]
    else:
        db_card = Enemy(sub_type=db_subtype,
                         title=wiki_card.title,
                         description=wiki_card.description,
                         encounter_nr=wiki_card.encounter_nr,
                         strength=wiki_card.strength,
                         craft=wiki_card.craft)
        db_card.save()
        #print 'Dadano {0}'.format(db_card)
    return db_card

def get_or_create_object(wiki_card, db_subtype):
    db_card = Object.objects.filter(sub_type=db_subtype,
                                    title=wiki_card.title,
                                    encounter_nr=wiki_card.encounter_nr,
                                    description=wiki_card.description)
    if len(db_card) > 1:
        txt = 'Karta "{0}" wystepuje {1} razy!'.format(wiki_card.title, len(db_card))
        raise Exception(txt)
    elif len(db_card) == 1:
        db_card = db_card[0]
    else:
        db_card = Object(sub_type=db_subtype,
                         title=wiki_card.title,
                         description=wiki_card.description,
                         encounter_nr=wiki_card.encounter_nr)
        db_card.save()
        #print 'Dadano {0}'.format(db_card)
    return db_card

def get_or_create_magic_object(wiki_card, db_subtype):
    db_card = MagicObject.objects.filter(sub_type=db_subtype,
                                    title=wiki_card.title,
                                    encounter_nr=wiki_card.encounter_nr,
                                    description=wiki_card.description)
    if len(db_card) > 1:
        txt = 'Karta "{0}" wystepuje {1} razy!'.format(wiki_card.title, len(db_card))
        raise Exception(txt)
    elif len(db_card) == 1:
        db_card = db_card[0]
    else:
        db_card = MagicObject(sub_type=db_subtype,
                         title=wiki_card.title,
                         description=wiki_card.description,
                         encounter_nr=wiki_card.encounter_nr)
        db_card.save()
        #print 'Dadano {0}'.format(db_card)
    return db_card

def get_or_create_adventure(wiki_card, cardtype):
    db_card = cardtype.objects.filter(title=wiki_card.title,
                                      description=wiki_card.description)
    if len(db_card) > 1:
        txt = 'Karta "{0}" wystepuje {1} razy!'.format(wiki_card.title, len(db_card))
        raise Exception(txt)
    elif len(db_card) == 1:
        db_card = db_card[0]
    else:
        db_card = cardtype(title=wiki_card.title,
                           description=wiki_card.description,
                           encounter_nr=wiki_card.encounter_nr)
        db_card.save()
        #print 'Dadano {0}'.format(db_card)
    return db_card

def get_or_create_stranger(wiki_card):
    return get_or_create_adventure(wiki_card, Stranger)

def get_or_create_event(wiki_card):
    return get_or_create_adventure(wiki_card, Event)

def get_or_create_place(wiki_card):
    return get_or_create_adventure(wiki_card, Place)

def get_or_create_follower(wiki_card):
    return get_or_create_adventure(wiki_card, Follower)

def do_import(cardtype, category, exclude=None, wiki_section=0):
    category_name = category.replace('_', ' ')
    print category_name

    if cardtype == Enemy:
        db_subtype, created = EnemySubType.objects.get_or_create(name=category)
        if created:
            print 'Dodano:', category

    arts = wiki.category('Category_' + category, section=wiki_section)
    if exclude:
        for name in exclude:
            arts.remove(name)
    print len(arts)
    for art in arts:
        a = wiki.article(art)
        if cardtype in [Object, MagicObject]:
            created = False
            if a.card_subtype != '':
                db_subtype, created = ObjectSubType.objects.get_or_create(name=a.card_subtype)
            else:
                db_subtype, created = ObjectSubType.objects.get_or_create(name=a.card_type)
            if created:
                print 'Dodano:', db_subtype.name

        # weryfikacja podtypu dla wrogow
        if cardtype == Enemy:
            if a.card_subtype != category_name:
                print a.title, a.card_subtype

        if a.title == '':
            print Fore.RED + '{0} nie ma tytulu!'.format(art) + Fore.RESET
            a.title = art

        db_card = None
        if cardtype == Enemy:
            db_card = get_or_create_enemy(a, db_subtype)
        elif cardtype == Stranger:
            db_card = get_or_create_stranger(a)
        elif cardtype == Event:
            db_card = get_or_create_event(a)
        elif cardtype == Follower:
            db_card = get_or_create_follower(a)
        elif cardtype == Place:
            db_card = get_or_create_place(a)
        elif cardtype == Object:
            db_card = get_or_create_object(a, db_subtype)
        elif cardtype == MagicObject:
            db_card = get_or_create_magic_object(a, db_subtype)

        if db_card:
            check_sets(db_card, a, cardtype)

def count_cards():
    SET = Set.objects.get(name='Revised 4th Edition')
    count = 0

    def count_cardtype(cardtype):
        ret = 0
        cards = cardtype.objects.filter(sets=SET)
        for card in cards:
            for set in card.sets_set().all():
                ret += set.copies
        return ret

    count += count_cardtype(Enemy)
    count += count_cardtype(Event)
    count += count_cardtype(Follower)
    count += count_cardtype(Stranger)
    count += count_cardtype(Place)
    count += count_cardtype(Object)
    count += count_cardtype(MagicObject)
    print count

if __name__ == '__main__':
    wiki = TalismanWiki('c:\\_DATA_\\rozrywka\\talismanwiki')

    '''
    do_import(Enemy, 'Animal')
    do_import(Enemy, 'Construct')
    do_import(Enemy, 'Cultist')
    do_import(Enemy, 'Dragon')
    do_import(Enemy, 'Elemental')
    do_import(Enemy, 'Fae')
    do_import(Enemy, 'Law')
    do_import(Enemy, 'Monster', exclude=['Doppelganger'])
    do_import(Enemy, 'Norn')
    do_import(Enemy, 'Spirit')
    do_import(Stranger, 'Stranger')
    do_import(Event, 'Event')
    do_import(Follower, 'Follower')
    do_import(Place, 'Place')
    do_import(Object, 'Weapon')
    do_import(Object, 'Armour')
    do_import(Object, 'Object_(Revised_4th_Edition)', wiki_section=1)
    do_import(MagicObject, 'Magic_Object_(Revised_4th_Edition)', wiki_section=0)
    '''

    count_cards()
