# -*- coding: utf-8 -*-
from os.path import join
import re
import sys
from bs4 import BeautifulSoup


class WikiCard():
    def __init__(self):
        self.title = ''
        self.card_type = ''
        self.card_subtype = ''
        self.desciption = ''
        self.encounter_nr = 0
        self.strength = 0
        self.craft = 0

class TalismanWiki():
    def __init__(self):
        pass
        
    def article(self, name):
        if True:#try:
            ret = None
            f =  open(join('www.talismanwiki.com', name + '.html'), 'r')
            src = f.read()
            czy_karta = src.find('<h2><span class="mw-headline" id="Card_info">Card info</span></h2>') > -1
            
            if czy_karta:
                ret = WikiCard()
                title = re.compile('<p><b>Title</b>: <b>(.+?)</b>',re.M|re.DOTALL).findall(src)
                if title:
                    ret.title = title[0]
                
                type = re.compile('<p><b>Card Type</b>: <a href="(.+?)" title="(.+?)">(.+?)</a></p>',re.M|re.DOTALL).findall(src)
                if type:
                    ret.card_type = type[0][2]

                subtype = re.compile('<p><b>Sub Type</b>: <a href="(.+?)" title="(.+?)">(.+?)</a></p>',re.M|re.DOTALL).findall(src)
                if subtype:
                    ret.card_subtype = subtype[0][2]
              
                title_pos = src.find('<p><b>Title</b>: <b>')
                desc_pos_begin = src.find('\n', title_pos)
                desc_pos_end = src.find('<p><b>Encounter Number</b>', desc_pos_begin)
                html = src[desc_pos_begin:desc_pos_end]
                if html.find('<ol>'):
                    i = 0
                    while html.find('<li> ') > -1:
                        i += 1
                        html = html.replace('<li> ', '<li>{0}. '.format(i), 1)
                    soup = BeautifulSoup(html)
                    text = soup.get_text()
                ret.description = text.strip()

                encounter_nr = re.compile('<p><b>Encounter Number</b>: (.+?)</p>',re.M|re.DOTALL).findall(src)
                if encounter_nr:
                    ret.encounter_nr = int(encounter_nr[0])

                strength = re.compile('<p><b>Strength</b>: (.+?)</p',re.M|re.DOTALL).findall(src)
                if strength:
                    strength = strength[0]
                    if strength == '?':
                        strength = -1
                    else:
                        strength = int(strength)
                    ret.strength = strength
              
                craft = re.compile('<p><b>Craft</b>: (.+?)</p',re.M|re.DOTALL).findall(src)
                if craft:
                    craft = craft[0]
                    if craft == '?':
                        craft = -1
                    else:
                        craft = int(craft)
                    ret.craft = craft
                
                
                copies = re.compile('<p><b>Copies of this Card</b>: (.+?)   (.+)</p>',re.M|re.DOTALL).findall(src)
                if copies:
                    total = int(copies[0][0])
                    print total
                    sets = re.compile(' (.+?) in <a href="(.+?)" title="(.+?)">(.+?)</a>; ',re.M|re.DOTALL).findall(copies[0][1])
                    print len(sets)
                    for set in sets:
                        print set[0].strip, set[3].strip
                
                
                
            return ret
            
        #except:
        #    return None

if __name__ == '__main__':
    wiki = TalismanWiki()
    arts = [['Aegis', 'Academy', 'Ifrit_Invader'][2]]
    for art in arts:
        print ''
        print art
        a = wiki.article(art)
        #print a
        if a.__class__ == WikiCard:
            print 'title:', a.title
            print 'type:', a.card_type
            print 'subtype:', a.card_subtype
            print 'description:\n', a.description
            print 'encounter number:', a.encounter_nr
            print 'strength:', a.strength
            print 'craft:', a.craft
           
        print '-'*20

