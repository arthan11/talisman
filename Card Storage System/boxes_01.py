from PIL import Image, ImageDraw

TOP = 0
LEFT = 1
BOTTOM = 2
RIGHT = 3

class Shape(object):
    def __init__(self):
        self.shapes = []

    def draw_shapes(self):
        for shape in self.shapes:
            shape.draw_shape()

class Rectangle(Shape):
    def __init__(self, draw, x1, y1, x2, y2):
        super(Rectangle, self).__init__()
        self.x1, self.y1, self.x2, self.y2 = x1, y1, x2, y2
        self.draw = draw
        self.fill = "white"

    def add_rect(self, x1, y1, x2, y2, fill=None):
        shape = Rectangle(self.draw, x1, y1, x2, y2)
        self.shapes.append(shape)
        return shape

    def draw_shape(self):
        self.draw.rectangle((self.x1, self.y1, self.x2, self.y2), fill=self.fill, outline="black")
        super(Rectangle, self).draw_shapes()

class Trapeze(Shape):
    def __init__(self):
        super(Trapeze, self).__init__()

class Box(Shape):
    def __init__(self):
        super(Box, self).__init__()
        size = (450, 890)
        self.bg_color = (255,255,255,0)

        self.img = Image.new('RGBA', size, self.bg_color)
        self.draw = ImageDraw.Draw(self.img)

        fill = (122, 61, 0)

        center_rect = self.add_rect(95, 95, 445, 795)
        self.add_sides_to_rect(center_rect, 40, [TOP, LEFT, BOTTOM])
        left_rect = center_rect.shapes[1]
        left_rect.fill = fill
        self.add_trapezes_to_rect(left_rect, 50, [LEFT])

        #self.draw.polygon((95, 95-40, 95+350, 95-40, 95+350-50, 95-40-50, 95+50, 95-40-50), fill=fill, outline="black")

        self.draw_shapes()
        self.img.show()

    def add_sides_to_rect(self, rect, thickness, sides):
        for side in sides:
            if side == LEFT:
                rect.add_rect(rect.x1-thickness, rect.y1, rect.x1, rect.y2)
            elif side == RIGHT:
                rect.add_rect(rect.x2, rect.y1, rect.x2+thickness, rect.y2)
            elif side == TOP:
                rect.add_rect(rect.x1, rect.y1-thickness, rect.x2, rect.y1)
            elif side == BOTTOM:
                rect.add_rect(rect.x1, rect.y2, rect.x2, rect.y2+thickness)

    def add_trapezes_to_rect(self, rect, thickness, sides):
        for side in sides:
            if side == LEFT:
                rect.add_rect(rect.x1-thickness, rect.y1, rect.x1, rect.y2)
                self.add_trapeze(95, 95-40, 95+350, 95-40, 95+350-50, 95-40-50, 95+50, 95-40-50))
            elif side == RIGHT:
                rect.add_rect(rect.x2, rect.y1, rect.x2+thickness, rect.y2)
            elif side == TOP:
                rect.add_rect(rect.x1, rect.y1-thickness, rect.x2, rect.y1)
            elif side == BOTTOM:
                rect.add_rect(rect.x1, rect.y2, rect.x2, rect.y2+thickness)

    def add_rect(self, x1, y1, x2, y2, fill=None):
        #box = (x, y, x+width, y+height)
        shape = Rectangle(self.draw, x1, y1, x2, y2)
        self.shapes.append(shape)
        return shape
        #if not fill:
        #    fill = self.bg_color
        #self.draw.rectangle(box, fill=fill, outline="black")

    def add_trapeze(self, x1, y1, x2, y2, x3, y3, x4, y4):
        shape = Trapeze(self.draw, x1, y1, x2, y2)
        self.shapes.append(shape)
        return shape

if __name__ == '__main__':
    box = Box()
