from PIL import Image, ImageDraw


# positions
TOP = 0
LEFT = 1
BOTTOM = 2
RIGHT = 3

# orientations
PORTRAIT = 0
LANDSCAPE = 1

inch2cm = 2.54

# paper formats
A4 = 0

paper_sizes = [
        (21.0, 29.7),
    ]

CARD_WIDTH = 4.15
CARD_HEIGHT = 6.35

DPI = 300

def cm2px(cm):
    return int(round(cm * (DPI / inch2cm)))

class Shape(object):
    def __init__(self):
        self.shapes = []

    def draw_shapes(self):
        for shape in self.shapes:
            shape.draw_shape()

class Rectangle(Shape):
    def __init__(self, x1, y1, x2, y2):
        super(Rectangle, self).__init__()
        self.x1, self.y1, self.x2, self.y2 = x1, y1, x2, y2
        self.fill = "white"

    def add_shape(self, shape, side):
        #shape = Rectangle(self.draw, x1, y1, x2, y2)
        self.shapes.append({'shape': shape, 'side': side})
        return shape

    def draw(self, drw, x, y):
        x1, y1, x2, y2 = cm2px(x+self.x1), cm2px(y+self.y1), cm2px(x+self.x2), cm2px(y+self.y2)
        drw.rectangle((x1, y1, x2, y2), fill=self.fill, outline="black")
        #super(Rectangle, self).draw_shapes()

class Trapeze(Shape):
    def __init__(self):
        super(Trapeze, self).__init__()

class Box(Shape):
    def __init__(self):
        super(Box, self).__init__()
        size = (450, 890)
        self.bg_color = (255,255,255,0)

        self.img = Image.new('RGBA', size, self.bg_color)
        self.draw = ImageDraw.Draw(self.img)

        fill = (122, 61, 0)

        center_rect = self.add_rect(95, 95, 445, 795)
        self.add_sides_to_rect(center_rect, 40, [TOP, LEFT, BOTTOM])
        left_rect = center_rect.shapes[1]
        left_rect.fill = fill
        self.add_trapezes_to_rect(left_rect, 50, [LEFT])

        #self.draw.polygon((95, 95-40, 95+350, 95-40, 95+350-50, 95-40-50, 95+50, 95-40-50), fill=fill, outline="black")

        self.draw_shapes()
        self.img.show()

    def add_sides_to_rect(self, rect, thickness, sides):
        for side in sides:
            if side == LEFT:
                rect.add_rect(rect.x1-thickness, rect.y1, rect.x1, rect.y2)
            elif side == RIGHT:
                rect.add_rect(rect.x2, rect.y1, rect.x2+thickness, rect.y2)
            elif side == TOP:
                rect.add_rect(rect.x1, rect.y1-thickness, rect.x2, rect.y1)
            elif side == BOTTOM:
                rect.add_rect(rect.x1, rect.y2, rect.x2, rect.y2+thickness)

    def add_trapezes_to_rect(self, rect, thickness, sides):
        for side in sides:
            if side == LEFT:
                rect.add_rect(rect.x1-thickness, rect.y1, rect.x1, rect.y2)
                #self.add_trapeze(95, 95-40, 95+350, 95-40, 95+350-50, 95-40-50, 95+50, 95-40-50))
            elif side == RIGHT:
                rect.add_rect(rect.x2, rect.y1, rect.x2+thickness, rect.y2)
            elif side == TOP:
                rect.add_rect(rect.x1, rect.y1-thickness, rect.x2, rect.y1)
            elif side == BOTTOM:
                rect.add_rect(rect.x1, rect.y2, rect.x2, rect.y2+thickness)

    def add_rect(self, x1, y1, x2, y2, fill=None):
        #box = (x, y, x+width, y+height)
        shape = Rectangle(self.draw, x1, y1, x2, y2)
        self.shapes.append(shape)
        return shape
        #if not fill:
        #    fill = self.bg_color
        #self.draw.rectangle(box, fill=fill, outline="black")

    def add_trapeze(self, x1, y1, x2, y2, x3, y3, x4, y4):
        shape = Trapeze(self.draw, x1, y1, x2, y2)
        self.shapes.append(shape)
        return shape

class Paper(object):
    def __init__(self, format=A4, orientation=PORTRAIT, dpi=300):
        self.format = format
        self.dpi = dpi
        self.orientation = orientation
        self.bg_color = (255,255,255,0)

    def draw(self, shape, x=0, y=0):
        size = paper_sizes[self.format]
        size = (cm2px(size[0]), cm2px(size[1]))
        self.img = Image.new('RGBA', size, self.bg_color)
        drw = ImageDraw.Draw(self.img)

        shape.draw(drw, x, y)



if __name__ == '__main__':
    paper = Paper()

    box = Rectangle(0, 0, CARD_WIDTH+0.35, 7.0)
    box.fill = (122, 61, 0)

    #center_rect = box.add_rect(CARD_WIDTH)
    #center_rect.add_rect()

    paper.draw(box, 8, 10)
    paper.img.show()
