from PIL import Image, ImageDraw

# positions
center = 0
left = 1
right = 2
top = 3
bottom = 4
custom = 5

# orientations
PORTRAIT = 0
LANDSCAPE = 1

inch2cm = 2.54

# paper formats
A4 = 0

paper_sizes = [
        (21.0, 29.7),
    ]

CARD_WIDTH = 4.15
CARD_HEIGHT = 6.35
CARD_BOX_WIDTH = CARD_WIDTH + 0.2
CARD_BOX_HEIGHT = CARD_HEIGHT * 0.75

DPI = 300

def cm2px(cm):
    return int(round(cm * (DPI / inch2cm)))

def rotate_img(img, degree):
    size = list(img.size)
    if size[0] != size[1]:
        if size[0] > size[1]:
            to_cut = size[0] - size[1]
            size[1] = size[0]
        else:
            to_cut = size[1] - size[0]
            size[0] = size[1]
        new_img = Image.new("RGBA", size, (0, 0, 0, 0) )
        new_img.paste(img, (0, 0), img)
        new_img = new_img.rotate(degree)
        if degree == -90:
            new_img = new_img.crop((to_cut, 0, size[0], size[1]))
        if degree == 90:
            new_img = new_img.crop((0, 0, size[0] - to_cut, size[1]))
    else:
        new_img = img.rotate(degree)
    return new_img
 
class Page(object):
    def __init__(self, format=A4, orientation=PORTRAIT, dpi=300):
        self.format = format
        self.dpi = dpi
        self.orientation = orientation
        self.background = "white"
        size = paper_sizes[self.format]
        if orientation == PORTRAIT:
            size = (cm2px(size[0]), cm2px(size[1]))
        else:
            size = (cm2px(size[1]), cm2px(size[0]))
        self.img = Image.new("RGBA", size, self.background )

    def put(self, obj, position, rel_obj=None, pos=None):
        img = obj.to_img()
        x, y = 0, 0
        if position == center:
            x, y = self.img.size
            x =  (x - img.size[0]) / 2
            y =  (y - img.size[1]) / 2
        elif position == left:
            x, y = rel_obj.left - img.size[0] + 1, rel_obj.top
        elif position == right:
            x, y = rel_obj.left +  rel_obj.img.size[0] - 1, rel_obj.top
        elif position == top:
            x, y =  rel_obj.left, rel_obj.top - img.size[1] + 1
        elif position == bottom:
            x, y =  rel_obj.left, rel_obj.top + rel_obj.img.size[1] - 1
        elif position == custom:
            x, y = pos
            
        offset = (x, y, x + img.size[0], y + img.size[1])
        obj.left, obj.top = x, y
        self.img.paste(img, offset, img)
    
class Figure(object):
    def __init__(self):
        self.outline = 'black'
        self.fill = None
        self.background = None
        self.img = None
        self.left = 0
        self.top = 0
        self.center_img = None
        
class Rectangle(Figure):
    def __init__(self, width, height):
        super(Rectangle, self).__init__()
        self.width = width
        self.height = height
        self.x1, self.y1, self.x2, self.y2 = 0, 0, width, height

    def to_img(self):
        img = Image.new("RGBA", (self.width,self.height), (0, 0, 0, 0) )
        draw = ImageDraw.Draw(img)
        draw.rectangle((self.x1, self.y1, self.x2-1, self.y2-1), fill=None, outline=self.outline)
        if self.background:
            img_back = Image.open(self.background)
            img.paste(img_back, (0, 0, img_back.size[0], img_back.size[1]))
        draw.rectangle((self.x1, self.y1, self.x2-1, self.y2-1), fill=self.fill, outline=self.outline)
        if self.center_img:
            center_img = Image.open(self.center_img)
            center_img = rotate_img(center_img, -90)
            x = (self.width - center_img.size[0]) / 2
            y = (self.height - center_img.size[1]) / 2
            img.paste(center_img, (x, y, x + center_img.size[0], y + center_img.size[1]), center_img)

        self.img = img
        return img

class Trapeze(Figure):
    def __init__(self, width, height, connect_pos=right):
        super(Trapeze, self).__init__()
        self.width = width
        self.height = height
        self.connect_pos= connect_pos
        self.points = [
            0, 0,
            self.width-1, 0,
            self.width-1, self.height-1,
            0, self.height-1
        ]
        if connect_pos == bottom:
            self.points[0] = self.height - 1
            self.points[2] = self.width - self.height
        elif connect_pos == top:
            self.points[6] = self.height - 1
            self.points[4] = self.width - self.height
        elif connect_pos == right:
            self.points[1] = self.width - 1
            self.points[7] = self.height - self.width
        elif connect_pos == left:
            self.points[3] = self.width - 1
            self.points[5] = self.height - self.width

    def to_img(self):
        img = Image.new("RGBA", (self.width, self.height), (0, 0, 0, 0) )
        draw = ImageDraw.Draw(img)
        #draw.rectangle((self.x1, self.y1, self.x2-1, self.y2-1), fill=self.fill, outline=self.outline)
        draw.polygon(self.points, fill=self.fill, outline="black")
        self.img = img
        return img
        
if __name__ == '__main__':
    page = Page(orientation=LANDSCAPE)

    # box
    box_height = 6.85
    center_rect = Rectangle(cm2px(CARD_BOX_WIDTH), cm2px(box_height))
    center_rect.fill = (122, 61, 0)
    #page.put(center_rect, position=center)
    page.put(center_rect, position=custom, pos=(cm2px(8), cm2px(7)))

    left_rect = Rectangle(cm2px(CARD_BOX_HEIGHT), cm2px(box_height))
    left_rect.background = 'backgroundx2.png'
    page.put(left_rect, position=left, rel_obj=center_rect)

    right_rect = Rectangle(cm2px(CARD_BOX_HEIGHT), cm2px(box_height))
    right_rect.background = 'backgroundx2.png'
    page.put(right_rect, position=right, rel_obj=center_rect)


    top_rect = Rectangle(cm2px(CARD_BOX_WIDTH), cm2px(CARD_BOX_HEIGHT))
    top_rect.background = 'backgroundx2.png'
    page.put(top_rect, position=top, rel_obj=center_rect)

    bottom_rect = Rectangle(cm2px(CARD_BOX_WIDTH), cm2px(CARD_BOX_HEIGHT))
    bottom_rect.background = 'backgroundx2.png'
    page.put(bottom_rect, position=bottom, rel_obj=center_rect)

    trapeze_1 = Trapeze(cm2px(CARD_BOX_HEIGHT), cm2px(0.50), connect_pos=bottom)
    trapeze_1.fill = (122, 61, 0)
    page.put(trapeze_1, position=top, rel_obj=left_rect)

    trapeze_2 = Trapeze(cm2px(CARD_BOX_HEIGHT), cm2px(0.50), connect_pos=top)
    trapeze_2.fill = (122, 61, 0)
    page.put(trapeze_2, position=bottom, rel_obj=left_rect)

    trapeze_3 = Trapeze(cm2px(CARD_BOX_HEIGHT), cm2px(0.50), connect_pos=bottom)
    trapeze_3.fill = (122, 61, 0)
    page.put(trapeze_3, position=top, rel_obj=right_rect)

    trapeze_4 = Trapeze(cm2px(CARD_BOX_HEIGHT), cm2px(0.50), connect_pos=top)
    trapeze_4.fill = (122, 61, 0)
    page.put(trapeze_4, position=bottom, rel_obj=right_rect)


    # pocket
    rect_2 = Rectangle(cm2px(3.40), cm2px(box_height))
    rect_2.background = 'backgroundx2.png'
    rect_2.center_img = 'dungeon.png'
    page.put(rect_2, position=custom, pos=(cm2px(20), cm2px(7)) )

    left_rect_2 = Rectangle(cm2px(0.40), cm2px(box_height))
    left_rect_2.fill = (122, 61, 0)
    page.put(left_rect_2, position=left, rel_obj=rect_2)

    top_rect_2 = Rectangle(cm2px(3.40), cm2px(0.40))
    top_rect_2.background = 'backgroundx2.png'
    page.put(top_rect_2, position=top, rel_obj=rect_2)

    bottom_rect_2 = Rectangle(cm2px(3.40), cm2px(0.40))
    bottom_rect_2.background = 'backgroundx2.png'
    page.put(bottom_rect_2, position=bottom, rel_obj=rect_2)


    trapeze_2_1 = Trapeze(cm2px(3.40), cm2px(0.50), connect_pos=bottom)
    trapeze_2_1.fill = (122, 61, 0)
    page.put(trapeze_2_1, position=top, rel_obj=top_rect_2)

    trapeze_2_2 = Trapeze(cm2px(3.40), cm2px(0.50), connect_pos=top)
    trapeze_2_2.fill = (122, 61, 0)
    page.put(trapeze_2_2, position=bottom, rel_obj=bottom_rect_2)

    trapeze_2_3 = Trapeze(cm2px(0.50), cm2px(box_height), connect_pos=right)
    trapeze_2_3.fill = (122, 61, 0)
    page.put(trapeze_2_3, position=left, rel_obj=left_rect_2)

    '''
    # box
    center_rect = Rectangle(cm2px(14.50), cm2px(CARD_BOX_WIDTH))
    center_rect.fill = (247, 0, 0)
    #page.put(center_rect, position=center)
    page.put(center_rect, position=custom, pos=(cm2px(8), cm2px(7)))

    #left_rect = Rectangle(cm2px(CARD_BOX_HEIGHT), cm2px(6.85))
    #left_rect.background = 'backgroundx2.png'
    #page.put(left_rect, position=left, rel_obj=center_rect)
    '''



    page.img.show()
