# -*- coding: utf-8 -*-
import sys
from os import listdir
from os.path import isfile, join


mypath = 'cards'
files = listdir(mypath)#[f for f in listdir(mypath) if isfile(join(mypath, f))]

data_21 = ''
data_3 = ''

with open('_cards_18.sla', 'r') as myfile:
    data_21 = myfile.read()

with open('_cards_3.sla', 'r') as myfile:
    data_3 = myfile.read()

for i in range(len(files)/21):
    filename = '{:02}'.format(i+1)

    data1 = str(data_21)
    data2 = str(data_21)
    for j in range(18):
        src_filename1 = files[i*21 + j]
        src_filename2 = files[i*21 + (j/6+1)*6 - (j % 6) - 1]
        data1 = data1.replace('PFILE=""', 'PFILE="cards\{}"'.format(src_filename1), 1)
        data2 = data2.replace('PFILE=""', 'PFILE="cards\{}"'.format(src_filename2), 1)

    with open(filename + '_01.sla', 'w') as myfile:
        myfile.write(data1)
    with open(filename + '_01b.sla', 'w') as myfile:
        myfile.write(data2)



    data = str(data_3)
    for j in range(3):
        src_filename = files[i*21 + 18 + j]
        data = data.replace('PFILE=""', 'PFILE="cards\{}"'.format(src_filename), 1)

    with open(filename + '_02.sla', 'w') as myfile:
        myfile.write(data)