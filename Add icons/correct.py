import glob, os
from PIL import Image

curr_dir = os.path.dirname(os.path.abspath(__file__))

ikona = Image.open(curr_dir+'\\ikona\\stake-hammer1.png')
ikon_pos_up = [0, 2, 5, 11, 13, 29, 32]


os.chdir(curr_dir+"\\PNG PL")
for i, filename in enumerate(glob.glob("*.png")):
    print filename
    card_filename = curr_dir+'\\PNG PL\\'+filename
    new_card_filename = curr_dir+'\\PNG PL Poprawka\\'+filename
    img = Image.open(card_filename)
    #new_img = Image.new('RGB', (490, 750), 'white')
    new_img = Image.open(curr_dir+'\\frame.png')
    if i in ikon_pos_up:
        img.paste(ikona, (-4, 275), ikona)
    else:
        img.paste(ikona, (-4, 390), ikona)
    
    x = (490 - img.size[0]) / 2
    y = (750 - img.size[1]) / 2
    new_img.paste(img, (x, y))
    
    
    new_img.save(new_card_filename)
    #print pos