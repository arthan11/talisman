## Narzędzia związane z grą Talisman.
* Add icons - dodawanie ikony dodatku na kartach
* Card Storage System - generator pudełek na karty
* EON Utils - import i modyfikowanie kart w programie Strange Eons
* kalendarz - Generowanie kalendarza na dany miesiąc
* SLA Editor - układanie kart do druku przy wykorzystaniu programu Scribus
* Talisman Wiki - import danych o elementach gry z wikipedii Talismana