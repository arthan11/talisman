# -*- coding: utf-8 -*-
import datetime
import calendar
import locale
import sys
from PIL import Image, ImageFont, ImageDraw

class GenCal(object):
    def gen(self):
        locale.setlocale(locale.LC_ALL, 'polish') # 'pl_PL'
        now = datetime.datetime.today()
        #print now.month
        #first_weekday = datetime.date(now.year, now.month, 1).weekday()
        #print first_weekday
        month_name = calendar.month_name[now.month].decode('cp1250').upper()
        #month_name = 'pazdziernik'
        month_days = calendar.monthcalendar(now.year, now.month)
        
        # img
        width, height = 3335, 2407
        wh_ratio = float(width) / float(height)
        filename = 'test.png'
        img = Image.new( 'RGBA', (width, height), "white") 
        font_color = (255, 255, 255) #(0, 0, 0)
        draw = ImageDraw.Draw(img)
        
        # background
        box = [0, 0, width, height]
        #bg_filename = 'talisman_by_ralphhorsley.jpg'
        bg_filename = 'y-mir-rear.jpg'
        background = Image.open(r'backgrounds\{}'.format(bg_filename))
        bg_wight, bg_height = background.size
        bg_wh_ratio = float(bg_wight) / float(bg_height)
        print wh_ratio, bg_wh_ratio
        if wh_ratio >= bg_wh_ratio:
            new_w, new_h = width, width / bg_wh_ratio
        else:
            new_w, new_h = height * bg_wh_ratio, height
        print new_w, new_h
        background = background.resize((int(round(new_w)), int(round(new_h))), Image.ANTIALIAS)
        x, y = 0, 0
        background = background.crop((box[0]+x, box[1]+y, box[2]+x, box[3]+y))
        img.paste(background, box)
        
        # title
        title = month_name
        font = ImageFont.truetype(r"fonts\Windlass PL.ttf", 80)
        txt_size = font.getsize(title)
        #x = (width - txt_size[0]) / 2
        x, y = img.size
        x -= 720
        y -= 620
        if len(month_days) == 6:
            y = y - 90

        draw.text((x, y), title, font_color, font=font)
        
        # days
        font = ImageFont.truetype(r"fonts\Windlass PL.ttf", 60)
        x, y = img.size
        x -= 900
        y -= 500
        w, h = 120, 90
        if len(month_days) == 6:
            y = y - 90
        for i, row in enumerate(month_days):
            for j, day in enumerate(row):
                if day != 0:
                    draw.text((x+w*j, y+h*i), str(day).rjust(2, ' '), font_color, font=font)
        
        
        # logo
        logo = Image.open(r'logo-talisman-shadow.png')
        w, h = logo.size
        w, h = int(round(w*1.5)), int(round(h*1.5))
        x, y = img.size[0] - w - 80, 1380
        box = [x, y, x+w, y+h]
        logo = logo.resize((w, h), Image.ANTIALIAS)
        img.paste(logo, box, logo)
        
        img.save(filename) 

if __name__ == '__main__':
    cal = GenCal()
    cal.gen()