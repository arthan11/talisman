#-*- coding: utf-8 -*-
from PIL import Image
import pytesseract
import django
import os
import sys
from eon_mod import EonMod

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mim_turniej.settings")
django.setup()
from turniej.models import *

class EonCreator(object):
    def __init__(self, talia, typ_kart, rozszerzenie='Edycja Podstawowa', src_eon='src.eon', with_imgs=False):
        self.with_imgs = with_imgs
        self.with_defines = True
        self.talia = talia
        self.typ_karty = typ_kart
        self.rozszerzenie = rozszerzenie
        self.dst_folder = os.path.join('eon2', talia)
        self.db_talia = Talia.objects.get(nazwa=talia)
        self.db_karty = Karta.objects.filter(talia=self.db_talia)
        print '{}[{}]'.format(talia, len(self.db_karty))
        self.eon = EonMod(src_eon)        

    def get_def(self, text, name):
        p1 = text.find('<'+ name + ' ')
        if p1 < 0:
            return None
        txt = text[p1+1:]
        p2 = txt.find('>')
        txt = txt[:p2]
        return p1, p2, txt.split(' ')

    def without_defs(self, text):
        defs = [
            ['s', u'<b><size 10>S</size>IŁA<size 10>:</size> <size 10>{}</size></b>'],
            ['m', u'<b><size 10>M</size>OC<size 10>:</size> <size 10>{}</size></b>'],
            ['sm', u'<b><size 10>S</size>IŁA<size 10>:</size> <size 10>{}</size>, <size 10>M</size>OC<size 10>:</size> <size 10>{}</size></b>'],
        
        ]
        for d in defs:
            df = self.get_def(text, d[0])
            if df:
                #print df
                if len(df[2]) == 2:
                    new = d[1].format(df[2][1])
                elif len(df[2]) == 3:
                    new = d[1].format(df[2][1], df[2][2])
                txt = text[:df[0]] + new + text[df[1]+2:]
                return txt
        return text

    def create(self):
        for db_karta in self.db_karty:
            nazwa = db_karta.nazwa.title().encode('utf-8')
            typ_karty = self.typ_karty
            rozszerzenie = self.rozszerzenie
            nr_spotkania = str(db_karta.nr_spotkania)
            efekt = ''
            typ = ''

            db_typ = db_karta.typ.nazwa.encode('utf-8')
            db_podtyp = db_karta.podtyp.nazwa.encode('utf-8')       
            if db_typ == db_podtyp:
                typ = db_karta.typ.nazwa.title().encode('utf-8')
            elif db_typ == 'PRZEDMIOT' and db_podtyp == 'BROŃ':
                typ = 'Przedmiot'
                if self.with_defines:
                    efekt = '<bń>\n'
                else:
                    efekt = '<b><i>Broń</i></b>\n'
            elif db_typ == 'PRZEDMIOT' and db_podtyp == 'PANCERZ':
                typ = 'Przedmiot'
                if self.with_defines:
                    efekt = '<pz>\n'
                else:
                    efekt = '<b><i>Pancerz</i></b>\n'
            elif db_typ == 'MAGICZNY' and db_podtyp == 'PRZEDMIOT':
                typ = 'Magiczny Przedmiot'                        
            elif db_typ == 'MAGICZNY' and db_podtyp == 'PRZEDMIOT BROŃ':
                typ = 'Magiczny Przedmiot'
                if self.with_defines:
                    efekt = '<bń>\n'
                else:
                    efekt = '<b><i>Broń</i></b>\n'
            elif db_typ == 'MAGICZNY PRZEDMIOT' and db_podtyp == 'BROŃ':
                typ = 'Magiczny Przedmiot'
                if self.with_defines:
                    efekt = '<bń>\n'
                else:
                    efekt = '<b><i>Broń</i></b>\n'
            elif db_typ == 'PRZEDMIOT' and db_podtyp == 'MAGICZNY BROŃ':
                typ = 'Magiczny Przedmiot'
                if self.with_defines:
                    efekt = '<bń>\n'
                else:
                    efekt = '<b><i>Broń</i></b>\n'
            elif db_typ == 'MAGICZNY' and db_podtyp == 'PRZEDMIOT PANCERZ':
                typ = 'Magiczny Przedmiot'
                if self.with_defines:
                    efekt = '<pz>\n'
                else:
                    efekt = '<b><i>Pancerz</i></b>\n'
                    
                    
                    
            elif db_typ == 'PRZEDMIOT' and db_podtyp == 'NOSICIEL':
                typ = 'Przyjaciel'
            elif db_typ == 'PRZYJACIEL' and db_podtyp == 'NOSICIEL':
                typ = 'Przyjaciel'
            elif db_typ == 'PRZEDMIOT' and db_podtyp == 'DUBEL':
                typ = 'Przyjaciel'
            elif db_typ == 'PRZEDMIOT' and db_podtyp == 'SPECJALNY':
                typ = 'Przedmiot'
            elif db_typ == 'ZDARZENIE' and db_podtyp == 'STRÓŻ PRAWA':
                typ = 'Wróg - {}'.format(db_karta.podtyp.nazwa.title().encode('utf-8'))
                nr_spotkania = '2'
            
            
            
            elif db_typ == 'CZAR':
                #typ = 'Zaklęcie'
                typ = 'Czar - {}'.format(db_karta.podtyp.nazwa.title().encode('utf-8'))
            elif db_typ == 'DUCH' and db_podtyp == 'SMOK':
                typ = 'Wróg - Duch'
            elif db_typ == 'WRÓG': 
                typ = 'Wróg - {}'.format(db_karta.podtyp.nazwa.title().encode('utf-8'))
            else:
                print db_karta.id, db_karta.typ, ',', db_karta.podtyp
                raise Exception('nieznany typ karty!')
            
            
            self.eon.comment_chars('krzysztof.lemka', 'x')
            self.eon.set_nazwa(nazwa)
            self.eon.set_typ_karty(typ_karty)
            self.eon.set_typ(typ)
            self.eon.set_nr_spotkania(nr_spotkania)
            self.eon.set_rozszerzenie(rozszerzenie)
    
            if self.with_imgs:
                self.eon.set_img(r'turniej\static\img\old_1\{}\{}.png'.format(self.talia, db_karta.old_nr))
                self.eon.set_scale_50()
    
            
            opis = db_karta.opis
            if not self.with_defines:
                opis = self.without_defs(opis)
            
            #if efekt != '':
                #first_line_pos = opis.find('\n')
                #if first_line_pos > 0:
                #    opis = '<b>' + opis[:first_line_pos] + '</b>' + opis[first_line_pos:]
            efekt += opis.encode('utf-8')
            
            #efekt = opis.encode('utf-8')
            self.eon.set_efekt(efekt)
                
            
            if not os.path.exists(self.dst_folder):
                os.mkdir(self.dst_folder)
            for i in range(1):#db_karta.ilosc):
                filename = '{:03d} - {}.eon'.format(db_karta.old_nr+i, db_karta.nazwa.lower().encode('cp1250'))
                filename = os.path.join(self.dst_folder, filename)
            
                self.eon.save(filename)
        
if __name__ == '__main__':
    '''
    # SE2
    print ('\nSE2\n')
    eon_cre = EonCreator('ekwipunek', 'Karta Ekwipunku')
    eon_cre.create()
    eon_cre = EonCreator('talismany', 'Karta Talizmanu')
    eon_cre.create()
    eon_cre = EonCreator('otchlan', 'Karta Przygody')
    eon_cre.create()
    eon_cre = EonCreator('podziemia', 'Karta Podziemi')
    eon_cre.create()
    eon_cre = EonCreator('czary', 'Karta Zaklęcia')
    eon_cre.create()
    eon_cre = EonCreator('przygody', 'Karta Przygody')
    eon_cre.create()

    # SE3
    print ('\nSE3\n')
    
    eon_cre = EonCreator('ekwipunek', 'Karta Ekwipunku', src_eon=r'eon_src\3_ekwipunku.eon')
    eon_cre.dst_folder = os.path.join('eon3', eon_cre.talia)
    eon_cre.create()
    
    eon_cre = EonCreator('talismany', 'Karta Talizmanu', src_eon=r'eon_src\3_talismanu.eon')
    eon_cre.dst_folder = os.path.join('eon3', eon_cre.talia)
    eon_cre.create()
    
    eon_cre = EonCreator('otchlan', 'Karta Przygody', src_eon=r'eon_src\3_timescape.eon')
    eon_cre.dst_folder = os.path.join('eon3', eon_cre.talia)
    eon_cre.create()
    
    eon_cre = EonCreator('podziemia', 'Karta Podziemi', src_eon=r'eon_src\3_podziemi.eon')
    eon_cre.dst_folder = os.path.join('eon3', eon_cre.talia)
    eon_cre.create()
    
    eon_cre = EonCreator('czary', 'Karta Zaklęcia', src_eon=r'eon_src\3_zaklecia.eon')
    eon_cre.dst_folder = os.path.join('eon3', eon_cre.talia)
    eon_cre.create()
    
    eon_cre = EonCreator('przygody', 'Karta Przygody', src_eon=r'eon_src\3_przygody.eon')
    eon_cre.dst_folder = os.path.join('eon3', eon_cre.talia)
    eon_cre.create()
    '''
    # SE3 with old imgs
    print ('\nSE3 (with old imgs)\n')
    
    eon_cre = EonCreator('ekwipunek', 'Karta Ekwipunku', src_eon=r'eon_src\3_ekwipunku.eon', with_imgs=True)
    eon_cre.dst_folder = os.path.join('eon3_img', eon_cre.talia)
    eon_cre.with_defines = False
    eon_cre.create()
    
    eon_cre = EonCreator('talismany', 'Karta Talizmanu', src_eon=r'eon_src\3_talismanu.eon', with_imgs=True)
    eon_cre.dst_folder = os.path.join('eon3_img', eon_cre.talia)
    eon_cre.with_defines = False
    eon_cre.create()               
                                                                       #3_timescape
    eon_cre = EonCreator('otchlan', 'Karta Przygody', src_eon=r'eon_src\3_przygody.eon', with_imgs=True)
    eon_cre.dst_folder = os.path.join('eon3_img', eon_cre.talia)
    eon_cre.with_defines = False
    eon_cre.create()
    
    eon_cre = EonCreator('podziemia', 'Karta Podziemi', src_eon=r'eon_src\3_podziemi.eon', with_imgs=True)
    eon_cre.dst_folder = os.path.join('eon3_img', eon_cre.talia)
    eon_cre.with_defines = False
    eon_cre.create()
    
    eon_cre = EonCreator('czary', 'Karta Zaklęcia', src_eon=r'eon_src\3_zaklecia.eon', with_imgs=True)
    eon_cre.dst_folder = os.path.join('eon3_img', eon_cre.talia)
    eon_cre.with_defines = False
    eon_cre.create()
    
    eon_cre = EonCreator('przygody', 'Karta Przygody', src_eon=r'eon_src\3_przygody.eon', with_imgs=True)
    eon_cre.dst_folder = os.path.join('eon3_img', eon_cre.talia)
    eon_cre.with_defines = False
    eon_cre.create()
    
    