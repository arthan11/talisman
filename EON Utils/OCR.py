from PIL import Image
import pytesseract
import django
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mim_turniej.settings")
django.setup()
from turniej.models import *

def get_avg(img_rbg, x, y):
    r, g, b = img_rbg.getpixel((x, y))
    return (r + g + b) / 3

def is_rgb_avg_white(img_rbg, x, y):
    return get_avg(img_rbg, x, y) > 150

def get_pic_frame(img):
    img_rgb = img.convert('RGB')
           
    frame = [0]*4
    
    x, y = 130, 500
    while is_rgb_avg_white(img_rgb, x, y):
        x += 1
    x += 0
    frame[0] = x
    
    x, y = 1000, 500
    while is_rgb_avg_white(img_rgb, x, y):
        x -= 1
        avg = get_avg(img_rgb, x, y)
    x -= 2
    frame[2] = x
    
    y = 500
    while not is_rgb_avg_white(img_rgb, frame[0], y) or not is_rgb_avg_white(img_rgb, frame[2], y):
        y -= 1  
    frame[1] = y
    
    y = 500
    while not is_rgb_avg_white(img_rgb, frame[0], y) or not is_rgb_avg_white(img_rgb, frame[2], y):
        y += 1  
    frame[3] = y
    
    return frame 


if __name__ == '__main__':
    talia = 'przygody'
    
    
    db_talia = Talia.objects.filter(nazwa=talia)
    if len(db_talia):
        db_talia = db_talia[0]
    else:
       db_talia = Talia(nazwa=talia)
       #db_talia.save()

    txt_all = ''
    old_img_folder = r'turniej\static\img\old\{}'.format(talia)        
    for nr_karty in [265]:
        print(nr_karty)
        filename = r'png\{}\{}.png'.format(talia, nr_karty)
        
        img = Image.open(filename)
        
        pic_frame = get_pic_frame(img)
        print (pic_frame)                  
        img_pic = img.crop(pic_frame)
        #img_pic.show()
        img_pic = img_pic.resize((img_pic.width/2, img_pic.height/2), Image.ANTIALIAS)
        
        img_tytul = img.crop( (0, 230, img.width, 330) )
        txt_tytul = pytesseract.image_to_string(img_tytul, lang='pol')
        
        img_typ = img.crop( (0, 140, img.width, 230) )
        txt_typ = pytesseract.image_to_string(img_typ, lang='pol')
        txt_splitted = txt_typ.split()
     
        # usuniecie 1znakowych smieci
        print (txt_splitted)
        txt_splitted_new = []
        for t in txt_splitted:
            if len(t) <= 2:
                try:
                    int(t)
                except:
                    continue
            txt_splitted_new.append(t)
        txt_splitted = txt_splitted_new
        
        if len(txt_splitted) == 2:
            typ = txt_splitted[0]
            nr_spotkania = 0
            podtyp = ' '.join(txt_splitted[1:])
        else:
            try:
                typ = txt_splitted[0]
                nr_spotkania = int(txt_splitted[1])
                podtyp = ' '.join(txt_splitted[2:])
            except:
                typ = ' '.join(txt_splitted[:2])
                nr_spotkania = int(txt_splitted[2])
                podtyp = ' '.join(txt_splitted[3:])
                
        img_opis = img.crop( (0, pic_frame[3]+10, img.width, img.height) )
        txt_opis = pytesseract.image_to_string(img_opis, lang='pol')
        
        opis_lines = txt_opis.split('\n')
        opis = ''
        for i, line in enumerate(opis_lines):
            line = line.strip()
            if len(opis) > 0:
                if opis[-1] == '-':
                    opis = opis[:-1].strip()
                else:
                    if opis[-1] != ' ':
                        opis += ' '
            opis += line
        #print (opis)
        
        print ('tytul: ' + txt_tytul)
        print ('typ: ' + typ )
        print ('podtyp: ' + podtyp )
        print ('nr spotkania: {}'.format(nr_spotkania) )
        
        
        db_typ = Typ.objects.filter(nazwa=typ)
        if len(db_typ):
            db_typ = db_typ[0]
        else:
           db_typ = Typ(nazwa=typ)
           #db_typ.save()

        db_podtyp = Podtyp.objects.filter(nazwa=podtyp)
        if len(db_podtyp):
            db_podtyp = db_podtyp[0]
        else:
           db_podtyp = Podtyp(nazwa=podtyp)
           #db_podtyp.save()

        db_karta = Karta.objects.filter(
            nazwa = txt_tytul, 
            talia = db_talia, 
            typ = db_typ,
            podtyp = db_podtyp,
            nr_spotkania = nr_spotkania,
            opis = opis
        )
        if len(db_karta) > 0:
            if db_karta[0].old_nr != nr_karty:
                db_karta[0].ilosc += 1
                #db_karta[0].save()
        else:
            db_karta = Karta(
                nazwa = txt_tytul, 
                talia = db_talia, 
                typ = db_typ,
                podtyp = db_podtyp,
                nr_spotkania = nr_spotkania,
                opis = opis,
                old_nr = nr_karty
            )
        img_pic.save(r'{}\{}.png'.format(old_img_folder, nr_karty), dpi = (200, 200) )
        
            #db_karta.save()
        
        
        #txt_all += '\n{}\n{}\n{}\n{}\n'.format(i, txt_typ, txt_tytul, opis)
        #with open('x.txt', 'w+') as f:
        #    f.write(txt_all)
        
        #img.show()
    