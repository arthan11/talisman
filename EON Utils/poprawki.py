#-*- coding: utf-8 -*-
import django
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mim_turniej.settings")
django.setup()
from turniej.models import *

def wyszukaj_w_opisie(txt):
    db_karty = Karta.objects.filter(opis__contains=txt)
    print len(db_karty)
    db_karty_filtered = []
    for db_karta in db_karty:
        pos = db_karta.opis.encode('utf-8').find(txt)
        if pos > 0:
            db_karty_filtered.append(db_karta)
            print db_karta.talia, db_karta.old_nr, db_karta.nazwa
    print len(db_karty_filtered) 
    
def zamien(field_type, name_from, name_to):
    db_from = field_type.objects.get(nazwa=name_from)
    db_to = field_type.objects.get(nazwa=name_to)
    if field_type == Typ:
        db_karty = Karta.objects.filter(typ=db_from)
        db_karty.update(typ=db_to)
    elif field_type == Podtyp:
        db_karty = Karta.objects.filter(podtyp=db_from)
        db_karty.update(podtyp=db_to)
    db_from.delete()
    
if __name__ == '__main__':
    
    #zamien(Podtyp, 'NOŚICIEL', 'NOSICIEL')
    #zamien(Podtyp, 'NOŚSICIEL', 'NOSICIEL')
    #zamien(Podtyp, 'MIEISCE', 'MIEJSCE')
    #zamien(Podtyp, 'PRZEDMIOT --- -_ -a. --- -w', 'PRZEDMIOT')

    '''
    zamien(Podtyp, 'BRONX', 'BROŃ')
    zamien(Podtyp, 'NIFZNATOMY', 'NIEZNAJOMY')
    zamien(Podtyp, 'NIEŻNAJOMY', 'NIEZNAJOMY')
    zamien(Podtyp, 'DHCH', 'DUCH')
    zamien(Podtyp, 'POTWÓG', 'POTWÓR')
    zamien(Podtyp, '--- PRZEDMIOT', 'PRZEDMIOT')
    zamien(Podtyp, 'ODBIIALNY', 'ODBIJALNY')
    zamien(Podtyp, 'MIFISCF', 'MIEJSCE')
    zamien(Podtyp, 'PRZEDMIOT -=-', 'PRZEDMIOT')
    zamien(Podtyp, 'NIEZŻNAJOMY', 'NIEZNAJOMY')
    zamien(Podtyp, 'ZWIERZE', 'ZWIERZĘ')
    zamien(Podtyp, 'PRZEDMIOT BRONX', 'PRZEDMIOT BROŃ')
    zamien(Podtyp, 'PRZEDMIOT BROX', 'PRZEDMIOT BROŃ')

    zamien(Typ, 'NIFZNATIOMY', 'NIEZNAJOMY')
    zamien(Typ, 'NIEZŻZNAJOMY', 'NIEZNAJOMY')
    zamien(Typ, 'WPÓC', 'WRÓG')
    zamien(Typ, 'NIEZŻNAJOMY', 'NIEZNAJOMY')
    zamien(Typ, 'MIFEISCE', 'MIEJSCE')
    zamien(Typ, 'MIFISCE', 'MIEJSCE')
    zamien(Typ, 'NIEŻNAJOMY', 'NIEZNAJOMY')
    zamien(Typ, 'WRÓGC', 'WRÓG')
    zamien(Typ, 'MAAGICZNY', 'MAGICZNY')
    zamien(Typ, 'ZDERZENIE', 'ZDARZENIE')
    zamien(Typ, 'WRÓC', 'WRÓG')
    '''
    
    
    #wyszukaj_w_opisie('wytrzyma')
    #wyszukaj_w_opisie('poszukiwacz')
    #wyszukaj_w_opisie('przyjaci')